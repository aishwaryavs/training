package xyzbank;

import java.util.Date;

public class Account {
 private String ownerName;
 private Address address; 
 protected double balanceAmt;
 private Date date;
 private String status;
 

public Account(String ownerName, Address address, double balanceAmt, Date date, String status) {
	super();
	this.ownerName = ownerName;
	this.address = address;
	this.balanceAmt = balanceAmt;
	this.date = date;
	this.status = status;
}
public String getOwnerName() {
	return ownerName;
}
public void setOwnerName(String ownerName) {
	this.ownerName = ownerName;
}
public Address getAddress() {
	return address;
}
public void setAddress(Address address) {
	this.address = address;
}
public double getBalanceAmt() {
	return balanceAmt;
}
public void setBalanceAmt(double balAmt) {
	this.balanceAmt = balanceAmt;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
 
}

