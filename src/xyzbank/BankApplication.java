package xyzbank;

import java.util.Date;
import java.util.Scanner;

public class BankApplication {
	public static void main(String args[]) {
     Scanner sc=new Scanner(System.in);
     
	System.out.println("1.Savings 2. Current");
	int ch=sc.nextInt();
	switch(ch) {
	case 1: System.out.println("Enter your name");
	        String name=sc.nextLine();
	        System.out.println("Enter your Address");
	        System.out.println("Enter your City");
	        String city=sc.next();
	        System.out.println("Enter your State");
	        String state=sc.next();
	        System.out.println("Enter your pin");
	        long pin=sc.nextLong();
	        Address add=new Address(city,state,pin);
	        System.out.println("Enter your BalanceAmount");
	        double balance=sc.nextDouble();
	        
	        Date d=new Date(2021,9,29);
	        SavingsAccount savings=new SavingsAccount(name,add,balance,d,"Active");
	        System.out.println("Enter deposit amount");
	        double deposit=sc.nextDouble();
	        savings.deposit(deposit);
	        System.out.println("Enter withdraw amount");
	        double amt=sc.nextDouble();
	        System.out.println(savings.withdraw(amt));
	        
	case 2: System.out.println("Enter your name");
		    String cname=sc.nextLine();
		    System.out.println("Enter your Address");
		    System.out.println("Enter your City");
		    String ccity=sc.next();
		    System.out.println("Enter your State");
		    String cstate=sc.next();
		    System.out.println("Enter your pin");
		    long cpin=sc.nextLong();
		    Address cadd=new Address(ccity,cstate,cpin);
		    System.out.println("Enter your BalanceAmount");
		    double cbalance=sc.nextDouble();
		    
		    Date cd=new Date(2021,9,29);
		    CurrentAccount current=new CurrentAccount(cname,cadd,cbalance,cd,"Active");
		    System.out.println("Enter deposit amount");
		    double cdeposit=sc.nextDouble();
		    current.deposit(cdeposit);
		    System.out.println("Enter withdraw amount");
		    double camt=sc.nextDouble();
		    System.out.println(current.withdraw(camt));
	}
	
}
	}
	
